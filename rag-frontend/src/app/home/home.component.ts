import {Component, OnInit} from '@angular/core';
import {ApiService} from "../service/api.service";
import {FormBuilder, Validators} from "@angular/forms";
import {NotificationService} from "../service/notification.service";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit{
  constructor(private apiService:ApiService,private fb:FormBuilder,
              private notification:NotificationService) {
  }
  message:any;
  message1:any;
 file!:File


  selectFiles(event:any) {
    console.log(event)
    this.file = event.target.files[0]
  }

  upload(){
     const formData=new FormData();
     if(this.file){
       formData.append('file',this.file)
     }
     this.apiService.uploadDocument(formData)
       .subscribe({
         next:resp=>{
           this.message = resp.data;
           this.notification.info(this.message)
         },error:err=>{
           console.log("Error uploading "+err);
    }
       })
  }

  askLLM(){
     let object={
       query:this.askForm.value.query
     }
     this.apiService.askLLm(object)
       .subscribe({
         next:(resp:any)=>{
           this.message1 = resp.data
         },error:err=>{
           console.log("error ask llm "+err)
         }
       })
  }

  askForm=this.fb.group({
    query:['',Validators.required]
  })

  ngOnInit(): void {

  }

}
