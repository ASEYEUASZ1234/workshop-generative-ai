import { Injectable } from '@angular/core';
import {HttpBackend, HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiUrl = "http://localhost:9999/api/v1/llm"

  constructor(private http:HttpClient,
              private handler: HttpBackend,private http2:HttpClient) { }

  uploadDocument(data:any):Observable<any>{
    return this.postWithOption(this.apiUrl+'/upload',data);
  }

  askLLm(data:any):Observable<any>{
    return this.http.post(this.apiUrl+'/ask',data)
  }


  postWithOption(
    path: string,
    body: Object = {},
    httpHeaders?: any
  ): Observable<any> {
    this.http2 = new HttpClient(this.handler);
    // const token = this.tokenStorageService.getAccessToken();
    // let headers = {
    //   Authorization: `Bearer ${token}`,
    // };
    let headers={}
    if (httpHeaders) {
      headers = Object.assign(headers, httpHeaders);
    }
    return this.http2.post(path, body, {
      headers: headers,
    });
  }
}
