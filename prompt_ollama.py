from langchain_community.llms import Ollama 

llama_llm = Ollama(model="mistral",temperature=0)

system_message="""
Classify the sentiment of the review presented in the input as 'positive' or 'negative’
The review will be delimited by triple backticks that is ``` in the input.
Answer only 'positive' or 'negative’ 
Do not explain your answer.
"""

user_message_template ="```{review}```"
user_message1 ="I think that your services are very fine"
assistant_response1 ="positive"
user_message2 ="I do not like the food```"""
assistant_response2 ="negative"
user_message ="The look is bad"
few_shot_prompt = [
{"role":"system","content":system_message},
{"role":"user", "content":user_message_template.format(review=user_message1)},
{"role":"assistant", "content":assistant_response1},
{"role":"user", "content":user_message_template.format(review=user_message2)},
{"role":"assistant", "content":assistant_response2},
{"role":"user", "content":user_message_template.format(review=user_message)},
]
response = llama_llm.invoke(few_shot_prompt )
print(response)