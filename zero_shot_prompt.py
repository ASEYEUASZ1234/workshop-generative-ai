from langchain_openai import ChatOpenAI

OPEN_AI_KEY="........."

gpt4_llm = ChatOpenAI(model="gpt-3.5-turbo",temperature=0,api_key=OPEN_AI_KEY)

system_message =""" 
Classify the sentiment of the review presented in the input as 'positive' or 'negative’
The review will be delimited by triple backticks that is ``` in the input.
Answer only 'positive' or 'negative’ 
Do not explain your answer.
"""

user_message_template ="```{review}```"
user_message ="I think that your services are very bad"

zero_shot_prompt = [
{"role":"system","content":system_message},
{"role":"user", "content":user_message_template.format(review=user_message) },
]
response = gpt4_llm.invoke(zero_shot_prompt )
print(response.content)
