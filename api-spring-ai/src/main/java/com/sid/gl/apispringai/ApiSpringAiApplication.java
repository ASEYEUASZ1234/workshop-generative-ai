package com.sid.gl.apispringai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
public class ApiSpringAiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiSpringAiApplication.class, args);
	}

}
