package com.sid.gl.apispringai.config;

import jakarta.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

//@Configuration
//@EnableTransactionManagement
//@EnableJpaRepositories(
       // entityManagerFactoryRef = "documentEntityManagerFactory",
        //transactionManagerRef = "documentTransactionManager",
        //basePackages = {"com.sid.gl.apispringai.repositories.document"})

public class DocumentDataSourceConfiguration {

    //@Primary
    //@Bean(name="documentProperties")
    //@ConfigurationProperties("spring.datasource")
    public DataSourceProperties dataSourceProperties(){
        return new DataSourceProperties();
    }

    //@Primary
    //@Bean(name="documentDatasource")
    //@ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource(@Qualifier("documentProperties")DataSourceProperties properties){
        return properties.initializeDataSourceBuilder().build();
    }

    //@Primary
    //@Bean(name="documentEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean
            (EntityManagerFactoryBuilder builder,
             @Qualifier("documentDatasource") DataSource dataSource){

        return builder.dataSource(dataSource)
                .packages("com.sid.gl.apispringai.document")
                .persistenceUnit("users").build();
    }

    //@Primary
    //@Bean(name = "documentTransactionManager")
    //@ConfigurationProperties("spring.jpa")
    public PlatformTransactionManager transactionManager(
            @Qualifier("documentEntityManagerFactory") EntityManagerFactory entityManagerFactory) {

        return new JpaTransactionManager(entityManagerFactory);
    }
}
