package com.sid.gl.apispringai.dto;

import lombok.Data;

@Data
public class UserQueryDTO {
    private String query;
}
