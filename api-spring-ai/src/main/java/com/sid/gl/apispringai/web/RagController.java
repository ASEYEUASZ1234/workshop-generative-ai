package com.sid.gl.apispringai.web;

import com.sid.gl.apispringai.dto.UserQueryDTO;
import com.sid.gl.apispringai.service.RagService;
import com.sid.gl.apispringai.utils.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/llm")
@CrossOrigin(value = "*")
public class RagController {

    @Autowired
    private RagService ragService;

    @PostMapping(path  = "/upload",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ApiResponse> uploadFile(@RequestParam("file")MultipartFile file) throws Exception {
          ApiResponse apiResponse = new ApiResponse();
          apiResponse.setHttpStatusCode(HttpStatusCode.valueOf(200));
          apiResponse.setData(ragService.uploadForAskLLM(file));
          return new ResponseEntity<>(apiResponse,HttpStatus.OK);
    }

    @PostMapping("ask")
    public ResponseEntity<ApiResponse> askLLm(@RequestBody final UserQueryDTO userQueryDTO){
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setHttpStatusCode(HttpStatusCode.valueOf(200));
        apiResponse.setData(ragService.invokeLLM(userQueryDTO));
        return new ResponseEntity<>(apiResponse,HttpStatus.OK);
    }
}
