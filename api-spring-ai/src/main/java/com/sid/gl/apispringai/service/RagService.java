package com.sid.gl.apispringai.service;

import com.sid.gl.apispringai.dto.UserQueryDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ai.chat.ChatResponse;
import org.springframework.ai.chat.messages.Message;
import org.springframework.ai.chat.messages.UserMessage;
import org.springframework.ai.chat.prompt.Prompt;
import org.springframework.ai.chat.prompt.SystemPromptTemplate;
import org.springframework.ai.document.Document;
import org.springframework.ai.openai.OpenAiChatClient;
import org.springframework.ai.openai.OpenAiChatOptions;
import org.springframework.ai.openai.api.OpenAiApi;
import org.springframework.ai.reader.pdf.PagePdfDocumentReader;
import org.springframework.ai.reader.pdf.config.PdfDocumentReaderConfig;
import org.springframework.ai.transformer.splitter.TokenTextSplitter;
import org.springframework.ai.vectorstore.VectorStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RagService {
    @Autowired
    private VectorStore vectorStore;

    @Value("${spring.ai.openai.api-key}")
    private String OPENAPI_KEY;

    public String invokeLLM(UserQueryDTO query){
        log.info("Process invoke response by ask llm --{} ",query.getQuery());
        List<Document> documentList = vectorStore.similaritySearch(query.getQuery());
        String systemTemplateMessage =
                """
                         Answer the following question based only on the provide CONTEXT
                        If the answer is not found in the context, respond "I don't know".
                        CONTEXT : 
                        {CONTEXT}
                        
                        """;
        String context = documentList
                .stream()
                .map(d->d.getContent())
                .collect(Collectors.joining("\n"));
        Message message =
                new SystemPromptTemplate(systemTemplateMessage)
                        .createMessage(Map.of("CONTEXT",documentList));
        UserMessage userMessage = new UserMessage(query.getQuery());
        Prompt prompt = new Prompt(List.of(message,userMessage));
        OpenAiApi openAiApi =
                new OpenAiApi(OPENAPI_KEY);

        OpenAiChatOptions options =
                OpenAiChatOptions.builder()
                        .withModel("gpt-3.5-turbo")
                        .build();

        OpenAiChatClient openAiChatClient =
                new OpenAiChatClient(openAiApi,options);

        ChatResponse response = openAiChatClient.call(prompt);
        String responseContent = response.getResult().getOutput().getContent();
        log.info("Response sended by prompt {} ",responseContent);
        return responseContent;
    }

    

    public String uploadForAskLLM(MultipartFile file) throws Exception {
        Path folderPath = Paths.get("./uploads").toAbsolutePath().normalize();
        try{
            Files.createDirectories(folderPath);
        }catch (Exception e){
            throw new Exception("Could not create the directory where the uploaded files will be stored.", e);
        }
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        Path filePath = folderPath.resolve(fileName);
        Files.copy(file.getInputStream(),filePath, StandardCopyOption.REPLACE_EXISTING);

        //embeddinText after upload
        Resource fileResource = new FileSystemResource(new File("./uploads/"+fileName));
        embeddingText(fileResource);

        return "Le fichier est bien uploadé et sauvegardé dans la base de données vector";
    }

    private void embeddingText(Resource pdfResource){
        PdfDocumentReaderConfig config=PdfDocumentReaderConfig.defaultConfig();
        PagePdfDocumentReader pagePdfDocumentReader =
                new PagePdfDocumentReader(pdfResource,config);
        List<Document> documents = pagePdfDocumentReader.get();
        String content = documents.stream()
                .map(Document::getContent)
                .collect(Collectors.joining("\n"));
        TokenTextSplitter tokenTextSplitter =
                new TokenTextSplitter();
        List<String> chunks = tokenTextSplitter.split(content,1000);
        List<Document> chunkDocs= chunks.stream()
                .map(Document::new).collect(Collectors.toList());
        vectorStore.accept(chunkDocs);
    }


}
