package com.sid.gl.apispringai.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

//@Configuration
public class AiRagDataSourceConfiguration {
    //@Bean(name = "aiProperties")
    @ConfigurationProperties("spring.datasource.aidb")
    public DataSourceProperties dataSourceProperties(){
        return new DataSourceProperties();
    }

    //@Bean(name="aiDatasource")
    @ConfigurationProperties(prefix = "spring.datasource.aidb")
    public DataSource dataSource(@Qualifier("aiProperties") DataSourceProperties properties){
        return properties.initializeDataSourceBuilder().build();
    }

}
