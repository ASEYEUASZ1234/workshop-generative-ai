package com.sid.gl.apispringai.utils;

import lombok.Data;
import org.springframework.http.HttpStatusCode;

@Data
public class ApiResponse<T> {
    private HttpStatusCode httpStatusCode;
    private T data;
}
