from langchain_groq import ChatGroq

GROQ_API_KEY="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"

groq_llm = ChatGroq(model_name="mixtral-8x7b-32768",temperature=0,api_key=GROQ_API_KEY)

system_message =""" 
Classify the sentiment of the review presented in the input as 'positive' or 'negative’
The review will be delimited by triple backticks that is ``` in the input.
Answer only 'positive' or 'negative’ 
Do not explain your answer.
"""

user_message_template ="```{review}```"
user_message1 ="I think that your services are very fine"
assistant_response1 ="positive"
user_message2 ="I do not like the food```"""
assistant_response2 ="negative"
user_message ="The look is bad"
few_shot_prompt = [
{"role":"system","content":system_message},
{"role":"user", "content":user_message_template.format(review=user_message1)},
{"role":"assistant", "content":assistant_response1},
{"role":"user", "content":user_message_template.format(review=user_message2)},
{"role":"assistant", "content":assistant_response2},
{"role":"user", "content":user_message_template.format(review=user_message)},
]
response = groq_llm.invoke(few_shot_prompt )
print(response.content.replace("</s>",""))